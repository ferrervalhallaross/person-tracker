export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAyd1egNiTsbQ9P01A5VDcF1xDQtDH8Aqs",
    authDomain: "person-tracker-app.firebaseapp.com",
    databaseURL: "https://person-tracker-app.firebaseio.com",
    projectId: "person-tracker-app",
    storageBucket: "person-tracker-app.appspot.com",
    messagingSenderId: "569024097152",
    appId: "1:569024097152:web:70c7121e3017560073ef65",
    measurementId: "G-DCWFWW3TCQ"
  }
};
