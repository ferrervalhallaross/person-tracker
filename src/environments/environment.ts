// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAyd1egNiTsbQ9P01A5VDcF1xDQtDH8Aqs",
    authDomain: "person-tracker-app.firebaseapp.com",
    databaseURL: "https://person-tracker-app.firebaseio.com",
    projectId: "person-tracker-app",
    storageBucket: "person-tracker-app.appspot.com",
    messagingSenderId: "569024097152",
    appId: "1:569024097152:web:70c7121e3017560073ef65",
    measurementId: "G-DCWFWW3TCQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
