import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    private snapshotChangesSubscription: any;

    constructor(
        public afs: AngularFirestore,
        public afAuth: AngularFireAuth
    ) { }

    updateLocation(id, location, timestamp) {
        let property = `locations.${timestamp}`;
        return this.afs.doc<any>('persons/' + id).update({
            [property]: location
        })
    }


    addPerson(body) {
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('persons').add(body).then(
                res => resolve(res),
                err => reject(err))
        })
    }

    getBorrowerInfo(id) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.doc<any>('borrower/' + id).valueChanges()
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err)
                })
        });
    }


    updateOneSignalID(id, osID) {
        return this.afs.doc<any>('borrower/' + id).update({
            oneSignalID: osID
        })
    }

    updateOneSignalIDAdmin(osID) {
        return this.afs.doc<any>('users/35kgU52oCoxxvWEFniT9').update({
            oneSignalID: osID
        })
    }

    updateFiles(id, url) {
        console.log('url', url);

        return this.afs.doc<any>('borrower/' + id).update({
            files: firebase.firestore.FieldValue.arrayUnion(url)
        })
    }

    getTasks() {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection('people').doc(currentUser.uid).collection('tasks').snapshotChanges();
                    resolve(this.snapshotChangesSubscription);
                }
            })
        })
    }

    getTask(taskId) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.doc<any>('people/' + currentUser.uid + '/tasks/' + taskId).valueChanges()
                        .subscribe(snapshots => {
                            resolve(snapshots);
                        }, err => {
                            reject(err)
                        })
                }
            })
        });
    }

    unsubscribeOnLogOut() {
        //remember to unsubscribe from the snapshotChanges
        try {
            this.snapshotChangesSubscription.unsubscribe();
        } catch (error) {
            console.log('error', error);

        }
    }

    updateTask(taskKey, value) {
        return new Promise<any>((resolve, reject) => {
            let currentUser = firebase.auth().currentUser;
            this.afs.collection('people').doc(currentUser.uid).collection('tasks').doc(taskKey).set(value)
                .then(
                    res => resolve(res),
                    err => reject(err)
                )
        })
    }

    deleteTask(taskKey) {
        return new Promise<any>((resolve, reject) => {
            let currentUser = firebase.auth().currentUser;
            this.afs.collection('people').doc(currentUser.uid).collection('tasks').doc(taskKey).delete()
                .then(
                    res => resolve(res),
                    err => reject(err)
                )
        })
    }

    createTask(value) {
        return new Promise<any>((resolve, reject) => {
            let currentUser = firebase.auth().currentUser;
            this.afs.collection('people').doc(currentUser.uid).collection('tasks').add({
                title: value.title,
                description: value.description,
                image: value.image
            })
                .then(
                    res => resolve(res),
                    err => reject(err)
                )
        })
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
            var aux: any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL("image/jpeg");
            callback(dataURL);
        };
        img.src = imageUri;
    };

    uploadImage(imageURI, randomId) {
        return new Promise<any>((resolve, reject) => {
            let storageRef = firebase.storage().ref();
            let imageRef = storageRef.child('image').child(randomId);
            this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(snapshot => {
                        snapshot.ref.getDownloadURL()
                            .then(res => resolve(res))
                    }, err => {
                        reject(err);
                    })
            })
        })
    }
}