import { Injectable } from "@angular/core";
import * as firebase from 'firebase/app';
import { FirebaseService } from './firebase.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private firebaseService: FirebaseService,
        public afAuth: AngularFireAuth,
        private storage: Storage,
        private afs: AngularFirestore,
    ) { }
    
    checkIfPerson(id) {
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('persons').where("id", "==", id)
            .get().then(function(querySnapshot) {
                resolve(querySnapshot)
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
                reject(error)
            });
        })  
    }


    doRegister(value) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
                .then(
                    res => resolve(res),
                    err => reject(err))
        })
    }

    addAdminUser(body, id){  
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('adminUsers').doc(id).set(body).then(
            res => resolve(res),
            err => reject(err))
        })
    }

    doLogin(value){
        return new Promise<any>((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(value.email, value.password)
            .then(
            res => resolve(res),
            err => reject(err))
        })
    }


    doLogout(){
        return new Promise((resolve, reject) => {
            this.afAuth.auth.signOut()
            .then(() => {
            this.firebaseService.unsubscribeOnLogOut();
            resolve();
            }).catch((error) => {
            reject(error);
            });
        })
    }

    getAdminEmail() {
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('users').where("userType", "==", "super-admin")
            .get().then(function(querySnapshot) {
                resolve(querySnapshot)
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
                reject(error)
            });
        })  
    }

}
