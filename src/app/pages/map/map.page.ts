import { Component, OnInit, ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { Coordinates, Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
    @ViewChild('map') mapElement: any;
    map: google.maps.Map;
    personList = [];
    markers = [];
    loading = false;
    focusAlready = false;

    myMarker: any;
    firstTime = true;

    myPosition = {
        x: 14.5995,
        y: 120.9842
    }

    isDestroyed = false;
    constructor(
        private geolocation: Geolocation,
        private alert: AlertController,
        private firebaseService: FirebaseService,
        private storage: Storage
    ) { }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.isDestroyed = true;
    }

    ngAfterViewInit(): void {
        const mapProperties = {
            center: new google.maps.LatLng(14.5995, 120.9842),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: true,
            fullscreenControl: false,
            maxZoom: 40,
            minZoom: 5,
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        this.getMyPosition();
    }

    getMyPosition() {

        console.log('getMyPosition');
        const options = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 0
        };
        if (true) {
            console.log('if true');
            this.geolocation.getCurrentPosition(options)
                .then((position) => {
                    console.log('getCurrentPosition', position);
                    this.myPosition.x = position.coords.latitude
                    this.myPosition.y = position.coords.longitude


                    let { x, y } = this.myPosition
                    const myLatLng = new google.maps.LatLng(x, y);
                    if (this.firstTime) {
                        this.map.setZoom(12);
                        this.map.panTo(myLatLng);
                        this.firstTime = false;
                        this.myMarker = new google.maps.Marker({
                            position: myLatLng,
                            map: this.map,
                            icon: this.marker()
                        });
                    } else {
                        this.myMarker.setPosition(myLatLng);
                    }

                    setTimeout(() => {
                        if(!this.isDestroyed) {
                            this.updateLocation(position.coords);
                            this.getMyPosition();
                        }
                    }, 5000);
                })
                .catch((error) => {
                    console.log('location error', error);
                });
        } else {

            console.log('getMyPosition fail');
            alert("Geolocation is not supported by this browser.");
        }
    }

    async updateLocation(coords: Coordinates) {
        const id = await this.storage.get('personId');
        if(Math.abs(coords.latitude) && Math.abs(coords.longitude)) {
            const location = new firebase.firestore.GeoPoint(coords.latitude, coords.longitude)
            this.firebaseService.updateLocation(id, location, Date.now())
        }
    }

    setMarkers() {
        for (let i = 0; i < this.personList.length; i++) {
            if (this.markers.length <= i) {
                const marker = new google.maps.Marker({
                    position: new google.maps.LatLng(this.personList[i].x, this.personList[i].y),
                    map: this.map,
                    icon: this.marker()
                });
                this.markers.push({ marker, icon: this.marker() })

            } else {
                if (
                    this.markers[i].marker.getPosition().lat() != this.personList[i].x
                    || this.markers[i].marker.getPosition().lng() != this.personList[i].y
                ) {
                    this.markers[i].marker.setPosition(new google.maps.LatLng(this.personList[i].x, this.personList[i].y));
                }

                if (
                    this.markers[i].icon.name != this.marker().name
                ) {
                    this.markers[i].marker.setIcon(this.marker())
                    this.markers[i].icon = this.marker();
                }
            }
        }
        if (!this.focusAlready) this.focus();
    }

    focus() {
        this.focusAlready = true;
        var bounds = new google.maps.LatLngBounds();

        for (let i = 0; i < this.personList.length; i++) {
            bounds.extend(new google.maps.LatLng(this.personList[i].x, this.personList[i].y));
        }

        this.map.fitBounds(bounds);
    }

    focusTo(bus) {
        this.map.panTo(new google.maps.LatLng(bus.x, bus.y));
    }

    marker() {
        return {
            path: google.maps.SymbolPath.CIRCLE,
            fillColor: '#f04141',
            fillOpacity: 0.5,
            strokeColor: '#d33939',
            strokeOpacity: 1,
            strokeWeight: 2,
            scale: 7,
            name: 'red',
        }
    }

    showAlert(msg) {
        this.alert.create({
            message: msg
        }).then(alert => {
            alert.present();
        })
    }
}