import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AlertController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {validations_form: FormGroup;

    validation_messages = {
      'email': [
        { type: 'required', message: 'Email is required.' },
        { type: 'pattern', message: 'Enter a valid email.' }
      ],
      'password': [
        { type: 'required', message: 'Password is required.' },
        { type: 'minlength', message: 'Password must be at least 5 characters long.' }
      ],
      'address': [
        { type: 'required', message: 'Address is required.' },
      ],
      'firstName': [
        { type: 'required', message: 'First Name is required.' },
      ],
      'lastName': [
        { type: 'required', message: 'Last Name is required.' },
      ],
      'middleInitial': [
        { type: 'required', message: 'Middle Initial is required.' },
      ],
      'mobileNumber': [
        { type: 'required', message: 'Mobile Number is required.' },
        { type: 'minlength', message: 'Mobile Number must be at least 11 digits long.' }
      ],
      'incomeMonthly': [
        { type: 'required', message: 'Monthly Income is required.' },
      ],
      'incomeProof': [
        { type: 'required', message: 'Proof of Income is required.' },
      ],
      'guarantorName': [
        { type: 'required', message: 'Guarantor Name is required.' },
      ],
      'guarantorRelation': [
        { type: 'required', message: 'Relation is required.' },
      ],
      'guarantorMobile': [
        { type: 'required', message: 'Guarantor mobile number is required.' },
      ],
    };
  
    constructor(
      private authService: AuthService,
      private formBuilder: FormBuilder,
      private router: Router,
      private alert: AlertController,
      private fireStorage: AngularFireStorage,
      private afs: AngularFirestore,
      private fireService: FirebaseService,
      private storage: Storage
    ) { }
  
    async ngOnInit() {
      this.validations_form = this.formBuilder.group({
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),
        password: new FormControl('', Validators.compose([
          Validators.minLength(5),
          Validators.required
        ])),
        // address: new FormControl('', Validators.compose([
        //   Validators.required
        // ])),
        // dateCreated: new FormControl(Date.now(), Validators.compose([
        //   Validators.required
        // ])),
        // firstName: new FormControl('', Validators.compose([
        //   Validators.required
        // ])),
        // lastName: new FormControl('', Validators.compose([
        //   Validators.required
        // ])),
        // middleInitial: new FormControl('', Validators.compose([
        //   Validators.required
        // ])),
        // mobileNumber: new FormControl(undefined, Validators.compose([
        //   Validators.required, Validators.minLength(11), Validators.maxLength(11)
        // ])),
        // incomeMonthly: new FormControl('', Validators.compose([
        //   Validators.required
        // ])),
        // incomeProof: new FormControl(0, Validators.compose([
        //   // Validators.required
        // ])),
        // oneSignalID: new FormControl('', Validators.compose([
        //   // Validators.required
        // ])),
        // guarantorName:  new FormControl('', Validators.compose([
        //   Validators.required
        // ])),
        // guarantorRelation: new FormControl('', Validators.compose([
        //   Validators.required
        // ])),
        // guarantorMobile: new FormControl(undefined, Validators.compose([
        //   Validators.required, Validators.minLength(11), Validators.maxLength(11)
        // ])),
      });
  
    //   const oneSignalID = await this.storage.get('oneSignalID');
    //   this.validations_form.controls['oneSignalID'].setValue(oneSignalID);
    }
  
    tryRegister(){
      let id = this.afs.createId();
      const authBody = {
          email: this.validations_form.value.email,
          password: this.validations_form.value.password
      }
  
      // firebase.auth().onAuthStateChanged(user => {
      // })
  
      this.authService.doRegister(authBody)
      .then(res => {
          console.log(res);
          res.user.sendEmailVerification();
          this.authService.addAdminUser(this.validations_form.value, res.user.uid).then(data => {
                  console.log("Success add company", data);
                  this.showAlert("Your account has been created. Please check your email for verification.");
                  this.goLoginPage();
                  firebase.auth().signOut();
          }).catch( error => {
                  // this.errorMessage = error.message;
                  this.showAlert(error.message);
          })
      }, err => {
          console.log(err);
          this.showAlert(err.message);
      })
    }
  
    goLoginPage(){
      this.router.navigate(["/login"]);
    }
  
    showAlert(message) {
      this.alert.create( {
        message,
        buttons: [
          {
            text: 'Okay',
            role: 'Cancel',
            handler: () => {
              console.log('Confirm Okay');
            }
          }
        ]
      }).then(alert => {
        alert.present();
      })
    }
}
