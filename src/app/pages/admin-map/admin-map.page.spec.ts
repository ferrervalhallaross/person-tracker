import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdminMapPage } from './admin-map.page';

describe('AdminMapPage', () => {
  let component: AdminMapPage;
  let fixture: ComponentFixture<AdminMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
