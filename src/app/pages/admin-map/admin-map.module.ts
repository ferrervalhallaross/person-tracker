import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminMapPageRoutingModule } from './admin-map-routing.module';

import { AdminMapPage } from './admin-map.page';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminMapPageRoutingModule,
    IonicStorageModule.forRoot()
  ],
  declarations: [AdminMapPage]
})
export class AdminMapPageModule {}
