import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';

@Component({
    selector: 'app-admin-map',
    templateUrl: './admin-map.page.html',
    styleUrls: ['./admin-map.page.scss'],
})
export class AdminMapPage implements OnInit {
    @ViewChild('map') mapElement: any;
    map: google.maps.Map;
    personList = [];
    markers = [];
    loading = false;
    focusAlready = false;

    myMarker: any;
    firstTime = true;

    myPosition = {
        x: 14.5995,
        y: 120.9842
    }
    constructor(
        private geolocation: Geolocation,
        private alert: AlertController,
        private firebaseService: FirebaseService,
        private storage: Storage,
        private ngZone: NgZone
    ) { }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        const mapProperties = {
            center: new google.maps.LatLng(14.5995, 120.9842),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: true,
            fullscreenControl: false,
            maxZoom: 40,
            minZoom: 5,
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        // this.listenToPersonList();
        this.getMyPosition();
        this.listenToPersonList();
    }

    getMyPosition() {

        console.log('getMyPosition');
        const options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        if (true) {
            console.log('if true');
            this.geolocation.getCurrentPosition(options)
                .then((position) => {
                    console.log('getCurrentPosition', position);
                    this.myPosition.x = position.coords.latitude
                    this.myPosition.y = position.coords.longitude
                    let { x, y } = this.myPosition
                    const myLatLng = new google.maps.LatLng(x, y);
                    if (this.firstTime) {
                        this.map.setZoom(12);
                        // this.map.panTo(myLatLng);
                        this.firstTime = false;
                        this.myMarker = new google.maps.Marker({
                            position: myLatLng,
                            map: this.map,
                            icon: this.marker()
                        });
                    } else {
                        this.myMarker.setPosition(myLatLng);
                    }

                    // setTimeout(() => {
                    //     this.getMyPosition();
                    // }, 5000);
                })
                .catch((error) => {
                    console.log('location error', error);
                });
        } else {

            console.log('getMyPosition fail');
            alert("Geolocation is not supported by this browser.");
        }
    }

    setMarkers() {
        for (let i = 0; i < this.personList.length; i++) {
            if (!this.personList[i].locations) {
                continue;
            }
            // console.log('locations', this.personList[i].locations, Object.entries(this.personList[i].locations).sort((a: any, b: any) => {return a[0] - b[0]}));
            const location: any = Array.from(Object.entries(this.personList[i].locations).sort((a: any, b: any) => { return a[0] - b[0] })).pop();
            const lat = location[1].latitude
            const lng = location[1].longitude
            console.log('locations 2', location, lat, lng);

            if (this.markers.length <= i) {
                console.log('here');

                const marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    map: this.map,
                    icon: this.marker('blue')
                });
                console.log('marker', marker.getPosition().lat(), marker.getPosition().lng());

                this.markers.push({ marker, icon: this.marker('blue') })

            } else {
                if (
                    this.markers[i].marker.getPosition().lat() != lat
                    || this.markers[i].marker.getPosition().lng() != lng
                ) {
                    this.markers[i].marker.setPosition(new google.maps.LatLng(lat, lng));
                }

                if (
                    this.markers[i].icon.name != this.marker().name
                ) {
                    this.markers[i].marker.setIcon(this.marker('blue'))
                    this.markers[i].icon = this.marker('blue');
                }
            }
        }
        if (!this.focusAlready) this.focus();
    }



    async listenToPersonList() {
        this.loading = true;
        const adminUid = await this.storage.get('adminUid');
        firebase.firestore().collection('persons').where("adminUid", "==", adminUid)
            .onSnapshot((querySnapshot) => {
                const data = []
                querySnapshot.forEach(function (doc) {
                    data.push({ ...doc.data(), _id: doc.id })
                });
                console.log('new data', data);
                this.loading = false;
                this.ngZone.run(() => {
                    this.personList = data
                })
                this.setMarkers();
            })
    }

    focus() {
        this.focusAlready = true;
        var bounds = new google.maps.LatLngBounds();

        for (let i = 0; i < this.personList.length; i++) {

            if (!this.personList[i].locations) {
                continue;
            }
            const location: any = Array.from(Object.entries(this.personList[i].locations).sort((a: any, b: any) => { return a[0] - b[0] })).pop();
            const lat = location[1].latitude
            const lng = location[1].longitude
            bounds.extend(new google.maps.LatLng(lat, lng));
        }

        this.map.fitBounds(bounds);
    }

    focusTo(person) {
        if (person.locations) {
            const location: any = Array.from(Object.entries(person.locations).sort((a: any, b: any) => { return a[0] - b[0] })).pop();
            const lat = location[1].latitude
            const lng = location[1].longitude
            this.map.panTo(new google.maps.LatLng(lat, lng));
        } else {
            this.showAlert('This person does not have any locations yet')
        }
    }

    marker(color = '#f04141') {
        return {
            path: google.maps.SymbolPath.CIRCLE,
            fillColor: color,
            fillOpacity: 1,
            strokeColor: 'white',
            strokeOpacity: 1,
            strokeWeight: 2,
            scale: color == '#f04141' ? 4 : 7,
            name: 'red',
        }
    }

    async add() {
        const alert = await this.alert.create({
            cssClass: 'my-custom-class',
            header: 'Add Person',
            inputs: [
                {
                    name: 'id',
                    type: 'text',
                    id: 'name-id',
                    value: this.randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
                    placeholder: 'USER ID',
                    disabled: true
                },
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Name'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: async (data) => {
                        console.log('Confirm Ok', data);
                        let body = {
                            ...data,
                            adminUid: await this.storage.get('adminUid')
                        }
                        if (data.name) {
                            this.firebaseService.addPerson(body)
                        } else {
                            this.showAlert('Please add a name')
                        }
                    }
                }
            ]
        });

        await alert.present();
    }


    showAlert(msg) {
        this.alert.create({
            message: msg
        }).then(alert => {
            alert.present();
        })
    }


    randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }
}
