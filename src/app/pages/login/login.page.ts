import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgForm, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AdminService } from 'src/app/services/admin.service';
import * as firebase from 'firebase/app';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    validations_form: FormGroup;
    validations_form_2: FormGroup;
    loading = false;

    validation_messages = {
        'email': [
            { type: 'required', message: 'Email is required.' },
            { type: 'pattern', message: 'Please enter a valid email.' }
        ],
        'password': [
            { type: 'required', message: 'Password is required.' },
            { type: 'minlength', message: 'Password must be at least 5 characters long.' }
        ]
    };

    validation_messages_2 = {
        'userId': [
            { type: 'required', message: 'User ID is required.' },
            { type: 'minlength', message: 'User ID must be 6 characters long.' }
        ]
    };

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private router: Router,
        private alert: AlertController,
        private storage: Storage,
        private menu: MenuController,
        private fireService: FirebaseService,
        private adminService: AdminService
    ) {
    }

    ngOnInit() {
        this.menu.enable(false, 'client');
        this.menu.enable(false, 'admin');
        this.validations_form = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.minLength(5),
                Validators.required
            ])),
        });


        this.validations_form_2 = this.formBuilder.group({
            userId: new FormControl('', Validators.compose([
                Validators.minLength(6),
                Validators.maxLength(6),
                Validators.required
            ])),
        });
    }

    trackMe() {
        this.menu.enable(false, 'admin');
        this.menu.enable(true, 'client');

        this.authService.checkIfPerson(this.validations_form_2.value.userId).then(data => {
            console.log('check if person', data.docs);
            if (data.size) {
                this.loading = false;
                this.storage.set('personId', data.docs[0].id).then(() => {
                    this.router.navigate(["/map"]);
                })
            } else {
                this.loading = false;
                return this.showAlert('This person no longer exists');
            }
        }).catch(error => {
            console.log('Error', error);
        });
    }

    tryLogin(value) {
        this.menu.enable(true, 'admin');
        this.menu.enable(false, 'client');
        this.loading = true;
        this.authService.doLogin(value)
            .then(res => {
                console.log('Succes Login', res);
                console.log('isEmailVerified', res.user.emailVerified);
                this.storage.set('adminUid', res.user.uid)
                this.adminMap();
            }, err => {
                this.loading = false;
                console.log('Error login', err);
                console.log(err)
            })
    }

    async unverifiedAlert() {
        const alert = await this.alert.create({
            cssClass: 'my-custom-class',
            header: 'Unverified Email!',
            message: 'Please verify your email before sign in!!!',
            buttons: [
                {
                    text: 'Resend',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        firebase.auth().currentUser.sendEmailVerification();

                        firebase.auth().signOut();
                    }
                }, {
                    text: 'Okay',
                    handler: () => {
                        console.log('Confirm Okay');
                    }
                }
            ]
        });

        await alert.present();
    }

    goRegisterPage() {
        this.router.navigate(["/signup"]);
    }

    showAlert(msg) {
        this.alert.create({
            message: msg
        }).then(alert => {
            alert.present();
        })
    }

    adminMap() {
        this.router.navigate(["/admin-map"]);
    }

    signup() {
        this.router.navigate(["/signup"]);
    }

}
