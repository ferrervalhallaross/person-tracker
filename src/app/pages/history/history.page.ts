import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
    @ViewChild('map') mapElement: any;
    map: google.maps.Map;
    personList = [];
    markers = [];
    loading = false;
    focusAlready = false;

    myMarker: any;
    firstTime = true;

    myPosition = {
        x: 14.5995,
        y: 120.9842
    }

    active = 0;
    activeTime = 0;
    constructor(
        private geolocation: Geolocation,
        private alert: AlertController,
        private firebaseService: FirebaseService,
        private storage: Storage,
        private ngZone: NgZone
    ) { }

    ngOnInit() {
    }

    changeActive(index) {
        this.active = index;
        this.activeTime = 0;
        // this.focusTo(0);
        this.setMarkers();
    }

    ngAfterViewInit(): void {
        const mapProperties = {
            center: new google.maps.LatLng(14.5995, 120.9842),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: true,
            fullscreenControl: false,
            maxZoom: 40,
            minZoom: 5,
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        // this.listenToPersonList();
        this.listenToPersonList();
    }

    setMarkers() {
        this.markers = [];
        this.personList[this.active].locationsSorted.map(location => {
            const lat = location[1].latitude
            const lng = location[1].longitude
            const marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: this.map,
                icon: this.marker('blue')
            });
            this.markers.push({ marker, icon: this.marker('blue') })
        })
        this.focus();
    }



    async listenToPersonList() {
        this.loading = true;
        const adminUid = await this.storage.get('adminUid');
        firebase.firestore().collection('persons').where("adminUid", "==", adminUid)
            .onSnapshot((querySnapshot) => {
                const data = []
                querySnapshot.forEach(function (doc) {
                    let locationsSorted = [];
                    if(doc.data().locations) {
                        locationsSorted = Array.from(Object.entries(doc.data().locations).sort((a: any, b: any) => { return a[0] - b[0] }))
                    }
                    data.push({ ...doc.data(), _id: doc.id, locationsSorted })
                });
                console.log('new data', data);
                this.loading = false;
                this.ngZone.run(() => {
                    this.personList = data
                })
                this.setMarkers();
            })
    }

    focus() {
        this.focusAlready = true;
        var bounds = new google.maps.LatLngBounds();

        this.personList[this.active].locationsSorted.map(location => {
            const lat = location[1].latitude
            const lng = location[1].longitude
            bounds.extend(new google.maps.LatLng(lat, lng));
        })

        this.map.fitBounds(bounds);
    }

    focusTo(index) {
        // console.log('this.personList[this.active]', this.personList[this.active]);
        
        try {
            this.activeTime = index;
            const location = this.personList[this.active].locationsSorted[index][1]
            const lat = location.latitude
            const lng = location.longitude
            this.map.panTo(new google.maps.LatLng(lat, lng));
        } catch (error) {
            console.log('error', error);
            
            this.showAlert('Error on getting location data')
        }
    }

    marker(color = '#f04141') {
        return {
            path: google.maps.SymbolPath.CIRCLE,
            fillColor: color,
            fillOpacity: 1,
            strokeColor: 'white',
            strokeOpacity: 1,
            strokeWeight: 2,
            scale: color == '#f04141' ? 4 : 7,
            name: 'red',
        }
    }

    showAlert(msg) {
        this.alert.create({
            message: msg
        }).then(alert => {
            alert.present();
        })
    }
}
